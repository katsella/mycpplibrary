**Only works on Linux**\
\
**Libraries**

- **AkList** 
  * Static list and LinkedList used. 
  * LinkedList keeps removed id list. 
  * When a new item aded, gets last removed items id from LinkedList
  * O(1) time complexity, when accessing to an item with id
  * When adding an item, you dont have to searh for empty index. You can simply find it from linkedlist.

* **StaticQueList**
  * Thread safe adding, removing

- **LinkedList**
  * Thread safe adding, removing

* **Dictionary**
  * Uses unordered_map
  * Thread safe adding, removing

- **Later**
  * Calls a callback function with a diffrent thread
  * Only 1 thread created for calling callbacks. If you add too many haevy callback, it's not gonna be efficient.
  * I'm using this for freeing memory after a thread finish it's job. Btw. you can't free a thread from the same thread. it's gonna lead thread to deadlock.

* **Athread**
  * You can run asynchronous function. If you set "Athread.Then(callback)", After function finished calls callback.
  * If your function has loop and you want to delete Athread safely. Then use wait in your loop. 

- **ThreadManager**
  * You can create multiple Athread.
  * Created Athread stored in LinkedList.
  * An Athread finish it's job deleted from LinkedList and memory.

* **TcpClient**
  * Created client socket can Send and Receive messages

- **ManagedTcpClient**
  * TcpClient with thread.

* **TcpServer**
  * TcpServer listen client, send and receive messages.

- **ManagedTcpServer**
  * TcpServer listens client in a loop from new a thread.
  * If a client connected, server create a new ManagedTcpClient and added to Aklist.
\
\
![TcpServerSS](/uploads/5931fe5b17eb54ec0864f02054ec5bc0/TcpServerSS.png)
  * Total Thread Count: 5
    * 1 Main Thread
    * 2 Client Thread
    * 1 Server Client Listening Thread
    * 1 Later Thread (used for deleting finished threads)
