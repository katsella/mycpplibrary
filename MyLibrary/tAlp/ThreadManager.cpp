#include "ThreadManager.h"

namespace alp
{
    ThreadManager::ThreadManager(Task tas)
        : _task(tas)
    {
        _running = 1;
    }

    void ThreadManager::Then(Callback task)
    {
        _callback = task;
    }

    void ThreadManager::Run(void *data)
    {
        if (_running == 0)
            return;
        struct threadItem<ThreadManager> *thItem = new struct threadItem<ThreadManager>();
        thItem->llItem = _llTasks.Push(thItem);
        thItem->t = this;
        thItem->data = data;
        // thItem->data = (void *)args...;
        pthread_create(&(thItem->th), NULL, ThreadManager::_thread, thItem);
    }

    void *ThreadManager::_thread(void *data)
    {
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
        pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

        struct threadItem<ThreadManager> *thItem = (struct threadItem<ThreadManager> *)(data);
        
        if (thItem->check.try_lock())
        {
            thItem->t->_task(thItem->data);
            thItem->t->_callback(thItem->data);
            thItem->t->_llTasks.Remove(thItem->llItem);
            delete thItem;
            pthread_t th = pthread_self();
            Later::Add([th]()
                       { pthread_join(th, NULL); });
        }

        return NULL;
    }

    ThreadManager::~ThreadManager()
    {
        _running = 0;
        _llTasks.Foreach([](_lLItem<struct threadItem<ThreadManager>> *item)
                         {
                             struct threadItem<ThreadManager> *thItem = (struct threadItem<ThreadManager> *)(item->ptr);
                             thItem->check.try_lock();
                             pthread_cancel(thItem->th);
                             thItem->t->_callback(thItem->data);
                             pthread_tryjoin_np(thItem->th, NULL);
                             //  pthread_join(thItem->th, NULL);

                             //       delete thItem;
                         });

        _llTasks.ClearAndDispose();
    }
}