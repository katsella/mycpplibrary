#ifndef ALP_THREADMANAGER
#define ALP_THREADMANAGER

#include <functional>
#include <pthread.h>
#include <unistd.h>
#include "Alp/Later.h"
#include "Alp/LinkedList.h"

namespace alp
{
    template <typename T>
    struct threadItem
    {
        pthread_t th;
        T *t;
        void *data;
        _lLItem<threadItem<T>> *llItem;
        std::mutex check;
    };

    class ThreadManager
    {
        typedef std::function<void(void *)> Callback;
        typedef std::function<void(void *)> Task;

    private:
        Callback _callback;
        Task _task;
        static void *_thread(void *data);
        alp::LinkedList<struct threadItem<ThreadManager>> _llTasks;
        int _running;

    public:
        ThreadManager(Task task);
        void Run(void *data);
        void Then(Callback task);
        ~ThreadManager();
    };
}
#endif