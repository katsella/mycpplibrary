#ifndef ALP_LINKED_LIST
#define ALP_LINKED_LIST

#include <iostream>
#include <mutex>
#include <functional>

namespace alp
{
    template <typename T>
    class _lLItem
    {
    public:
        _lLItem *prev, *next;
        T ptr;
    };

    template <typename T>
    class LinkedList
    {
    public:
        typedef std::function<void(T)> _lLForeachFunc;

    private:
        int _count = 0;
        std::mutex _lockList;

    public:
        _lLItem<T> *head, *tail;
        LinkedList();
        _lLItem<T> *Push(T ptr);
        int Add(_lLItem<T> item);
        int Remove(_lLItem<T> *item);
        T PopFirst();
        T PopLast();
        int ClearAndDispose();
        _lLItem<T> *GetAt(int index);
        void Foreach(_lLForeachFunc func);
        int TransferTo(LinkedList<T> *dest, _lLItem<T> *item);
        int GetCount();
        ~LinkedList();
    };
}

#endif