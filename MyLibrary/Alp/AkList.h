#ifndef AKLIST
#define AKLIST

#include <iostream>
#include <mutex>
#include "LinkedList.h"

namespace alp
{
    // keep disconnected client id in que
    // when new client connected, get id from the que if its avaible if not create new idLinkedList

    template <typename T>
    class AkListItem
    {
    public:
        T *item;
        _lLItem<T *> *llItem;
    };

    template <typename T>
    class AkList
    {
    private:
        int _max;
        std::mutex _lockList;
        AkListItem<T> **_listPtrs;
        int _listTail = 0;
        int _listCount = 0;

        int *_queEmptyPtrs;
        int _queHead = 0;
        int _queTail = 0;
        int _queCount = 0;

        LinkedList<T *> _llList;

    public:
        AkList(int max);
        int Add(T *ptr);
        T *Remove(int id);
        T *Get(int id);
        int GetCount();
        int GetMaxCount();
        void Foreach(std::function<void(T *)> callback);
        void ClearAndDispose();
        ~AkList();
    };
}
#endif