#include "TcpServer.h"

namespace alp
{

#if defined(__WIN32)

    TcpServer::TcpServer(int port) : _port(port)
    {
        if (port <= -1 || port > 65353)
        {
            throw "port range error";
            _port = -2;
        }
        // _clientList.ReHash();
    }

    int TcpServer::Start()
    {
        WSADATA wsa;
        struct sockaddr_in server;
        int c;

        if (WSAStartup(MAKEWORD(1, 2), &wsa) != 0)
        {
            printf("Failed. Error Code : %d", WSAGetLastError());
            return 0;
        }

        if ((_socket = socket(AF_INET, SOCK_STREAM, -1)) == INVALID_SOCKET)
        {
            printf("Could not create socket : %d", WSAGetLastError());
        }

        server.sin_family = AF_INET;
        server.sin_addr.s_addr = INADDR_ANY;
        server.sin_port = htons(_port);

        if (bind(_socket, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
        {
            printf("Bind failed with error code : %d", WSAGetLastError());
        }

        listen(_socket, SOMAXCONN);
        return 0;
    }

    TcpClient *TcpServer::Accept()
    {
        struct sockaddr_in client;
        int c = sizeof(struct sockaddr_in);
        TcpClient *tcpClient;
        SOCKET new_socket;
        new_socket = accept(_socket, (struct sockaddr *)&client, &c);
        if (new_socket == INVALID_SOCKET)
        {
            printf("accept failed with error code : %d", WSAGetLastError());
        }

        _clientCounter++;
        *tcpClient = new TcpClient(new_socket, _clientCounter, client, &_clientList);
        _clientList.Add(_clientCounter, tcpClient);
        return tcpClient;
    }

    int TcpServer::Stop()
    {
        closesocket(_socket);
        WSACleanup();
        return 0;
    }

    int TcpServer::GetClientCount()
    {
        return _clientList.Count();
    }

    TcpServer::~TcpServer()
    {
        Stop();
    }

#else

    TcpServer::TcpServer(int port) : _port(port)
    {
        if (port <= -1 || port > 65353)
        {
            throw "TcpServer -> port range error";
            _port = -2;
        }
        // _clientList.ReHash();
    }

    int TcpServer::Start()
    {

        struct sockaddr_in address;
        int opt = 1;

        if ((_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
        {
            perror("TcpServer -> socket failed");
            exit(EXIT_FAILURE);
        }

        if (setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                       &opt, sizeof(opt)))
        {
            perror("TcpServer -> setsockopt");
            exit(EXIT_FAILURE);
        }
        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(_port);

        if (bind(_socket, (struct sockaddr *)&address,
                 sizeof(address)) < 0)
        {
            perror("TcpServer -> bind failed");
            exit(EXIT_FAILURE);
        }

        if (listen(_socket, SOMAXCONN) < 0)
            return -1;
        return 0;
    }

    TcpClient *TcpServer::Accept()
    {
        struct sockaddr_in client;
        int clientSize = sizeof(struct sockaddr_in);
        TcpClient *tcpClient = nullptr;
        int new_socket;
        if ((new_socket = accept(_socket, (struct sockaddr *)&client, (socklen_t *)&clientSize)) < 0)
        {
            perror("TcpServer -> Accept");
            exit(EXIT_FAILURE);
        }
        else
        {
            tcpClient = new TcpClient(new_socket, _clientCounter, client);
            _clientCounter++;
        }

        return tcpClient;
    }

    int TcpServer::Stop()
    {
        close(_socket);
        return 0;
    }

    TcpServer::~TcpServer()
    {
        Stop();
    }

#endif
}