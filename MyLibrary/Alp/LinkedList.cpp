#include "LinkedList.h"

namespace alp
{
    template <typename T>
    LinkedList<T>::LinkedList()
    {
        head = new _lLItem<T>();
        tail = new _lLItem<T>();
        head->next = tail;
        head->prev = nullptr;
        tail->prev = head;
        tail->next = nullptr;
    }

    template <typename T>
    _lLItem<T> *LinkedList<T>::Push(T ptr)
    {
        _lockList.lock();
        _lLItem<T> *item = new _lLItem<T>();
        item->ptr = ptr;
        item->prev = tail->prev;
        item->prev->next = item;
        item->next = tail;
        tail->prev = item;
        _count++;
        _lockList.unlock();
        return item;
    }

    template <typename T>
    int LinkedList<T>::Add(_lLItem<T> item)
    {
        _lockList.lock();
        item->prev = tail->prev;
        item->prev->next = item;
        item->next = tail;
        tail->prev = item;
        _count++;
        _lockList.unlock();
        return 1;
    }

    template <typename T>
    int LinkedList<T>::Remove(_lLItem<T> *item)
    {
        _lockList.lock();
        if (_count == 0)
        {
            _lockList.unlock();
            return 0x0;
        }
        _lLItem<T> *left, *rigth;
        left = item->prev;
        rigth = item->next;
        left->next = rigth;
        rigth->prev = left;
        delete (item);
        _count--;
        _lockList.unlock();
        return 1;
    }

    template <typename T>
    T LinkedList<T>::PopFirst()
    {
        _lockList.lock();
        if (_count == 0)
        {
            _lockList.unlock();
            return nullptr;
        }
        auto item = head->next;
        T ptr = item->ptr;

        _lLItem<T> *left, *rigth;
        left = item->prev;
        rigth = item->next;
        left->next = rigth;
        rigth->prev = left;
        delete (item);
        _count--;
        _lockList.unlock();
        return ptr;
    }

    template <typename T>
    T LinkedList<T>::PopLast()
    {
        _lockList.lock();
        if (_count == 0)
        {
            _lockList.unlock();
            return nullptr;
        }
        auto item = tail->prev;
        T ptr = item->ptr;

        _lLItem<T> *left, *rigth;
        left = item->prev;
        rigth = item->next;
        left->next = rigth;
        rigth->prev = left;
        delete (item);
        _count--;
        _lockList.unlock();
        return ptr;
    }

    template <typename T>
    int LinkedList<T>::ClearAndDispose()
    {
        _lockList.lock();
        if (_count > 0)
        {
            _lLItem<T> *item = head->next;
            while ((item = item->next) != tail)
            {
                delete (item->prev->ptr);
                delete (item->prev);
            }
            delete (item->prev->ptr);
            delete (item->prev);
            head->next = tail;
            tail->prev = head;
            _count = 0;
        }
        _lockList.unlock();
        return 1;
    }

    template <typename T>
    _lLItem<T> *LinkedList<T>::GetAt(int index)
    {
        _lockList.lock();
        if (_count <= index)
            return nullptr;

        _lLItem<T> *item = head->next;

        for (size_t i = 0; i < index; i++)
        {
            item = item->next;
        }
        _lockList.unlock();
        return item;
    }

    template <typename T>
    void LinkedList<T>::Foreach(_lLForeachFunc func)
    {
        _lLItem<T> *item = head->next;
        _lLItem<T> *temp;
        while (item != tail)
        {
            temp = item->next;
            try
            {
                func(item->ptr);
            }
            catch (const std::exception &e)
            {
                std::cerr << e.what() << '\n';
            }

            item = temp;
        }
    }

    template <typename T>
    int LinkedList<T>::TransferTo(LinkedList<T> *dest, _lLItem<T> *item)
    {
        _lockList.lock();
        _lLItem<T> *left, *rigth;
        left = item->prev;
        rigth = item->next;
        left->next = rigth;
        rigth->prev = left;
        _count--;
        _lockList.unlock();
        dest->Add(item);
        return 1;
    }

    template <typename T>
    int LinkedList<T>::GetCount()
    {
        return _count;
    }

    template <typename T>
    LinkedList<T>::~LinkedList()
    {
        _lLItem<T> *item = head->next;
        if (item != nullptr)
        {
            item = item->next;
            while (item != nullptr)
            {
                delete ((item->prev));
                item = item->next;
            }
        }
        delete head;
        delete tail;
    }
}