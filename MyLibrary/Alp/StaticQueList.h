#ifndef ALP_QUELIST
#define ALP_QUELIST

#include <iostream>
#include <mutex>
#include <vector>

namespace alp
{
    template <typename T>
    class StaticQueList
    {
    private:
        T *_que;
        std::mutex _queLock;
        int _max;
        int _head;
        int _tail;
        int _count;

    public:
        StaticQueList(int size);
        int Push(const T &item);
        T PopLast();
        T PopFirst();
        int GetCount();
        ~StaticQueList();
    };
}
#endif