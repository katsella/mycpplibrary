#include "Later.h"

namespace alp
{
    Later::Later()
    {
        pthread_create(&_th, NULL, Later::CallbackLoop, this);
    }

    Later &Later::GetDeleteLater()
    {
        static Later instance;
        return instance;
    }

    void Later::Add(Later::Callback callback)
    {
        GetDeleteLater()._queDelete.Push(callback);
        GetDeleteLater().g_ready = true;
        GetDeleteLater().g_cv.notify_one();
    }

    void *Later::CallbackLoop(void *data)
    {
        pthread_detach(pthread_self());
        auto ths = (Later *)data;
        ths->_loopRunning = 1;
        while (ths->_loopRunning == 1)
        {

            std::unique_lock<std::mutex> u1(ths->g_mutex);
            ths->g_cv.wait(u1, [=]()
                           { return ths->g_ready; });
            while (ths->_queDelete.GetCount() > 0)
            {
                auto task = ths->_queDelete.PopFirst();
                task();
            }
            ths->g_ready = false;
        }

        return NULL;
    }

    void Later::Dispose()
    {
    }

    Later::~Later()
    {
        _loopRunning = 0;
        g_ready = true;
        g_cv.notify_one();

        pthread_join(_th, NULL);
    }
}