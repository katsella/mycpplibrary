
#ifndef DICTIONARY
#define DICTIONARY

#include <iostream>
#include <unordered_map>
#include <mutex>

namespace Alp
{
	class Dictionary
	{
	private:
		std::unordered_map<int, void *> *_list;
		std::mutex _lockList;

	public:
		~Dictionary()
		{
			free(_list);
		}
		Dictionary()
		{
			_list = new std::unordered_map<int, void *>;
		}
		int Add(int key, void *obj);
		void *Find(int key);
		int Remove(int key);
		int ReHash();
		int Count();
	};
}
#endif
