#include "AkList.h"

namespace alp
{

    template <typename T>
    AkList<T>::AkList(int max)
        : _max(max)
    {
        /*    _listPtrs = (uintptr_t *)malloc(sizeof(uintptr_t) * max);
        _queEmptyPtrs = (int *)malloc(sizeof(int) * max);*/

        _listPtrs = new AkListItem<T> *[max];
        _queEmptyPtrs = new int[max];
    }

    template <typename T>
    int AkList<T>::Add(T *ptr)
    {

        _lockList.lock();
        if (_listTail >= _max && _queCount <= 0)
        {
            _lockList.unlock();
            return -1;
        }
        int id = -1;
        if (_queCount == 0)
        {
            id = _listTail++;
        }
        else
        {
            id = _queEmptyPtrs[_queHead % _max];
            _queHead++;
            _queCount--;
        }
        AkListItem<T> *item = new AkListItem<T>();
        item->item = ptr;
        item->llItem = _llList.Push(ptr);

        _listPtrs[id] = item;
        _listCount++;

        _lockList.unlock();

        return id;
    }

    template <typename T>
    T *AkList<T>::Remove(int id)
    {

        _lockList.lock();
        if (id >= _listTail || _listPtrs[id] == nullptr)
        {
            _lockList.unlock();
            return nullptr;
        }
        _queEmptyPtrs[_queTail++ % _max] = id; // add removed id to the que
        _queCount++;

        AkListItem<T> *item = _listPtrs[id];

        T *ptr = item->item;
        _llList.Remove(item->llItem);
        delete item;

        _listPtrs[id] = nullptr;
        _listCount--;
        // printf("%p(%d) --> 0\n", &_listPtrs[id], id);
        _lockList.unlock();
        return ptr;
    }

    template <typename T>
    T *AkList<T>::Get(int id)
    {
        AkListItem<T> *item = _listPtrs[id];
        return item->item;
    }

    template <typename T>
    int AkList<T>::GetCount()
    {
        return _listCount;
    }

    template <typename T>
    int AkList<T>::GetMaxCount()
    {
        return _max;
    }

    template <typename T>
    void AkList<T>::Foreach(std::function<void(T *)> callback)
    {
        _llList.Foreach(callback);
    }

    template <typename T>
    void AkList<T>::ClearAndDispose()
    {
        _lockList.lock();
        AkListItem<T> *item;
        for (size_t i = 0; i < _listTail; i++)
        {
            item = _listPtrs[i];
            if (item != nullptr)
            {
                _llList.Remove(item->llItem);
                delete item->item;
                delete item;
            }
        }

        _listTail = 0;
        _listCount = 0;

        _queHead = 0;
        _queTail = 0;
        _queCount = 0;

        _lockList.unlock();
    }

    template <typename T>
    AkList<T>::~AkList()
    {
        delete[] _listPtrs;
        delete[] _queEmptyPtrs;
        printf("destroying... \n");
    }
}