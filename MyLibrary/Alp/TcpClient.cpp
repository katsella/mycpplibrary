#include "TcpClient.h"

namespace alp
{

#if defined(__WIN32)

    TcpClient::TcpClient(SOCKET socket, int clientId, struct sockaddr_in addr, Dictionary *clientList)
        : _socket(socket),
          _clientId(clientId),
          _addr(addr),
          _clientList(clientList)
    {
    }

    TcpClient::~TcpClient()
    {
    }

    int TcpClient::Send(char *data, int size)
    {
        send(_socket, data, size, 0);
        return 1;
    }

    int TcpClient::Receive(char *buffer, int size)
    {
        int x = -1;
        try
        {
            x = recv(_socket, buffer, size, 0);
        }
        catch (...)
        {
        }

        if (size <= 0)
        {
            Close();
            return size;
        }
        return x;
    }

    int TcpClient::Close()
    {
        _clientList->Remove(_clientId);
        closesocket(_socket);
        delete (this);
        return 1;
    }
#else

    TcpClient::TcpClient(int socket, int clientId, struct sockaddr_in addr)
        : _socket(socket),
          _clientId(clientId),
          _addr(addr)
    {
    }

    TcpClient::~TcpClient()
    {
        Close();
    }

    int TcpClient::Send(char *data, int size)
    {
        send(_socket, data, size, 0);
        return 1;
    }

    int TcpClient::GetSocket()
    {
        return _socket;
    }

    int TcpClient::Receive(char *buffer, int size)
    {
        int x = -1;

        x = recv(_socket, buffer, size, 0);

        if (x <= 0)
        {
            Close();
            return x;
        }
        return x;
    }

    int TcpClient::Close()
    {
        close(_socket);
        return 1;
    }

#endif
}