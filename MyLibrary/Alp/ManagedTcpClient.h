#ifndef ALP_MANAGEDTCPCLIENT
#define ALP_MANAGEDTCPCLIENT

#include "TcpClient.h"
#include "LinkedList.h"
#include <functional>
#include "Athread.h"

namespace alp
{
    typedef std::function<void(int clientId, char *buffer, int size)> ReceiveCallback;
    typedef std::function<void(int clientId)> DisconnectedCallback;
    typedef std::function<int(int clientId)> AcceptCallback;

    class ManagedTcpClient
    {
    public:
        enum ConnectionStatus
        {
            NOT_CONNECTED,
            CONNECTED
        };

        enum ReceiveStatus
        {
            NOT_RECEIVING,
            RECEIVING
        };

    private:
        TcpClient *_client;
        Athread *_AthreadClient;
        char *_buffer;
        int _bufferSize;
        ConnectionStatus _connectionStatus;
        ReceiveStatus _receiveStatus;
        AcceptCallback _acceptCallback;
        DisconnectedCallback _disconnectedEvent;
        ReceiveCallback _receiveEvent;
        int _disposed = 0;
        static void _Receive(void *data);
        int Disconnected();
        static void _Init(void *data);

    public:
        std::function<int(ManagedTcpClient *client)> initCallback;
        int _clientId;
        ManagedTcpClient(TcpClient *client, AcceptCallback acceptCallback, ReceiveCallback receiveEvent, DisconnectedCallback disconnectedEvent);
        int Init();
        int Receive(char *buffer, int size);
        int Send(char *buffer, int size);
        int GetSocket();
        int Close();
        ~ManagedTcpClient();
    };

}
#endif