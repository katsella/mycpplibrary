#include "StaticQueList.h"

namespace alp
{
    template <typename T>
    StaticQueList<T>::StaticQueList(int size) : _max(size)
    {
        _que = new T[size];
        _tail = 0;
        _head = 0;
        _count = 0;
    }

    template <typename T>
    int StaticQueList<T>::Push(const T &item)
    {
        _queLock.lock();
        int ret = 0;
        if (_count < _max)
        {
            _que[_tail++ % _max] = item;
            ret = 1;
            _count++;
        }
        _queLock.unlock();
        return ret;
    }

    template <typename T>
    T StaticQueList<T>::PopFirst()
    {
        _queLock.lock();
        if (_count == 0)
        {
            _queLock.unlock();
            return 0x0;
        }
        T item;
        if (_count > 0)
        {
            item = _que[_head % _max];
            _que[_head++ % _max] = 0x0;
            _count--;
        }
        _queLock.unlock();
        return item;
    }

    template <typename T>
    T StaticQueList<T>::PopLast()
    {
        _queLock.lock();
        if (_count == 0)
        {
            _queLock.unlock();
            return 0x0;
        }
        T item = nullptr;
        if (_count > 0)
        {
            item = _que[_tail % _max];
            _que[_tail-- % _max] = 0x0;
            _count--;
        }
        _queLock.unlock();
        return item;
    }

    template <typename T>
    int StaticQueList<T>::GetCount()
    {
        return _count;
    }

    template <typename T>
    StaticQueList<T>::~StaticQueList()
    {
        delete[](_que);
    }
}