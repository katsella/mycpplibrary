#include "Dictionary.h"

int Dictionary::Add(int key, void *obj)
{
	int state = -1;
	_lockList.lock();

	try
	{
		auto a = _list->insert({key, obj});
		//	free(&a);
		//	std::cout << _list->max_size();
		//a.first.
		//	a.first.
		
		state = 1;
	}
	catch (...)
	{
		std::cout << "----> hata \n";
	}

	_lockList.unlock();
	return state;
}

void *Dictionary::Find(int key)
{
	std::unordered_map<int, void *>::const_iterator item = _list->find(key);
	if (item == _list->end())
		return 0x0;
	else
		return item->second;
}

int Dictionary::Remove(int key)
{
	int state = -1;
	_lockList.lock();
	try
	{
		_list->erase(key);
		state = 1;
	}
	catch (int a)
	{
	}
	_lockList.unlock();
	return state;
}

int Dictionary::ReHash()
{
	int state = -1;
	_lockList.lock();
	try
	{
		_list->rehash(1024);
		state = 1;
	}
	catch (int a)
	{
	}
	_lockList.unlock();
	return state;
}

int Dictionary::Count()
{
	return _list->size();
}