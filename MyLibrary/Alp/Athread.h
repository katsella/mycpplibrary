#ifndef ALP_ATHREAD
#define ALP_ATHREAD

#include <pthread.h>
#include <functional>
#include <unistd.h>
#include "Alp/Later.h"

namespace alp
{
    typedef std::function<void(void *)> Task;

    class Athread
    {

    private:
        pthread_t pTh;
        Task _task;
        Task _callback;
        static void *_thread(void *data);
        int _isDone;
        void *_tItem;

    public:
        int canCancell;
        Athread(Task task);
        void Then(Task callback);
        void Run(void *arg);
        ~Athread();
    };

    class AthreadItem
    {
    public:
        Athread *aThread;
        void *data;
    };

}
#endif