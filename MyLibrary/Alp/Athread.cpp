#include "Athread.h"

namespace alp
{
    Athread::Athread(Task task)
        : _task(task)
    {
        _isDone = 0;
        _callback = 0x0;
        pTh = 0;
        _tItem = nullptr;
    }

    void Athread::Then(Task callback)
    {
        _callback = callback;
    }

    void Athread::Run(void *arg)
    {
        if (pTh == -1)
            return;
        AthreadItem *tItem = new AthreadItem();
        tItem->aThread = this;
        tItem->data = arg;
        _tItem = tItem;
        pthread_create(&pTh, NULL, Athread::_thread, _tItem);
    }

    void *Athread::_thread(void *data)
    {
        AthreadItem *tItem = (AthreadItem *)data;
        tItem->aThread->_task(tItem->data);

        tItem->aThread->_isDone = 1;

        pthread_t pTh = pthread_self();
        Later::Add([pTh]()
                   { pthread_join(pTh, NULL); });

        if (tItem->aThread->_callback != 0x0)
        {
            tItem->aThread->_callback(tItem->data);
            /* Later::Add([=]()
                       { tItem->aThread->_callback(tItem->data); });*/
        }
        return NULL;
    }

    Athread::~Athread()
    {
        AthreadItem *tItem = (AthreadItem *)_tItem;
        if (_isDone == 0 && pTh != 0)
        {
            pthread_cancel(pTh);
            pthread_join(pTh, NULL);
            if (_callback != 0x0)
                _callback(tItem->data);
        }
        pTh = -1;
        if (tItem != nullptr)
            delete tItem;
    }
}