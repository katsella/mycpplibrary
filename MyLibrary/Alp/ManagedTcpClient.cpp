#include "ManagedTcpClient.h"

namespace alp
{
    ManagedTcpClient::ManagedTcpClient(TcpClient *client, AcceptCallback acceptCallback, ReceiveCallback receiveCallback, DisconnectedCallback disconnectedCallback)
        : _acceptCallback(acceptCallback),
          _client(client),
          _disconnectedEvent(disconnectedCallback),
          _receiveEvent(receiveCallback)
    {
        _connectionStatus = CONNECTED;
        _bufferSize = 2048;
        _buffer = new char[_bufferSize];
        _receiveStatus = RECEIVING;
        _AthreadClient = new Athread(_Init);
        _AthreadClient->Then([](void *data)
                             {
                                 ManagedTcpClient *mClient = (ManagedTcpClient *)data;
                                 if (mClient->_disposed == 0)
                                     delete mClient;
                             });
    }

    int ManagedTcpClient::Init()
    {
        _AthreadClient->Run(this);
        return 1;
    }

    void ManagedTcpClient::_Init(void *data)
    {
        ManagedTcpClient *_this = (ManagedTcpClient *)data;
        if (_this->_acceptCallback != nullptr)
        {
            int accepted = -1;
            accepted = _this->_acceptCallback(_this->_clientId);
            if (accepted >= 0)
            {
                _this->_Receive(_this);
            }
        }
        else
        {
            _this->Close();
            delete _this;
        }
    }

    int ManagedTcpClient::Receive(char *buffer, int size)
    {
        return _client->Receive(buffer, size);
    }

    int ManagedTcpClient::Send(char *buffer, int size)
    {
        return _client->Send(buffer, size);
    }

    void ManagedTcpClient::_Receive(void *data)
    {
        ManagedTcpClient *_this = (ManagedTcpClient *)data;
        while (_this->_receiveStatus == RECEIVING)
        {
            int size = _this->_client->Receive(_this->_buffer, _this->_bufferSize);
            if (size <= 0)
            {
                // Disconnected();
                return;
            }
            _this->_receiveEvent(_this->_clientId, _this->_buffer, size);
        }
    }

    int ManagedTcpClient::GetSocket()
    {
        return _client->GetSocket();
    }

    int ManagedTcpClient::Disconnected()
    {

        if (_disconnectedEvent != nullptr)
            _disconnectedEvent(_clientId);

        return 1;
    }

    int ManagedTcpClient::Close()
    {
        _client->Close();
        return 1;
    }

    ManagedTcpClient::~ManagedTcpClient()
    {
        _disposed = 1;
        Disconnected();
        delete _AthreadClient;
        delete _client;
        delete[] _buffer;
    }
}