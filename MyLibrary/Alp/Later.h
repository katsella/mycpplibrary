#ifndef ALP_DELETELATER
#define ALP_DELETELATER

#include "LinkedList.h"
#include <pthread.h>
#include <functional>
#include <mutex>
#include <condition_variable>

namespace alp
{

    class Later
    {
    public:
        typedef std::function<void()> Callback;

    private:
        LinkedList<Callback> _queDelete;
        std::mutex g_mutex;
        std::condition_variable g_cv;
        bool g_ready = false;
        pthread_t _th;
        int _loopRunning;
        static void *CallbackLoop(void *data);

    public:
        Later(const Later &) = delete;
        Later();
        static Later &GetDeleteLater();
        static void Add(Later::Callback callback);
        static void Dispose();
        ~Later();
    };
}

#endif