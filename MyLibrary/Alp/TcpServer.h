#ifndef TCPSERVER
#define TCPSERVER
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#if defined(__WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#endif

#include "TcpClient.h"
#include <functional>

namespace alp
{
#if defined(__WIN32)
    class TcpServer
    {
    private:
        SOCKET _socket;
        int _port;
        Dictionary _clientList;
        int _clientCounter = 0;
        int _clientDisconnected();

    public:
        TcpServer(int port);
        ~TcpServer();
        int Start();
        int Stop();
        TcpClient *Accept();
        int GetClientCount();
    };
#else

    class TcpServer
    {
    private:
        int _socket;
        int _port;
        int _clientCounter = 0;
        int _clientDisconnected();

    public:
        TcpServer(int port);
        ~TcpServer();
        int Start();
        int Stop();
        TcpClient *Accept();
        int GetClientCount();
    };
#endif
}

#endif