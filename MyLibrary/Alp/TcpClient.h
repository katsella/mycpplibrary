#ifndef TCPCLIENT
#define TCPCLIENT

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#ifdef __WIN32
//#include <winsock2.h>

#else
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#endif
#include <functional>
#include <thread>

namespace alp
{

#ifdef __WIN32
    class TcpClient
    {
    private:
        SOCKET _socket;
        int _clientId;
        struct sockaddr_in _addr;

    public:
        TcpClient(SOCKET socket, int clientId, struct sockaddr_in addr);
        ~TcpClient();
        int Send(char *data, int size);
        int Receive(char *buffer, int size);
        int Close();
    };
#else
    class TcpClient
    {
    private:
        int _socket;
        int _clientId;
        struct sockaddr_in _addr;

    public:
        TcpClient(int socket, int clientId, struct sockaddr_in addr);
        ~TcpClient();
        int Send(char *data, int size);
        int Receive(char *buffer, int size);
        int GetSocket();
        int Close();
    };
#endif
}
#endif