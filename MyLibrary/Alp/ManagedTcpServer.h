#ifndef MANAGEDTCPSERVER
#define MANAGEDTCPSERVER
#include "TcpServer.h"
#include <functional>
#include "ManagedTcpClient.h"
#include "AkList.h"
#include "LinkedList.h"
#include "Athread.h"

namespace alp
{

    class ManagedTcpServer
    {
    public:
        enum ListeningStatus
        {
            LISTENING,
            NOT_LISTENING
        };

    private:
        TcpServer *tcpServer;
        Athread *_AthreadServer;
        int _disposed;

        int _max;
        AkList<ManagedTcpClient> *_listClient;
        LinkedList<ManagedTcpClient *> _linkedListClient;
        static void _Start(void *data);
        static void _Receive(void *data);

    public:
        ListeningStatus listeningStatus = NOT_LISTENING;
        ManagedTcpServer(int port, int max);
        int Start();
        AcceptCallback acceptEvent = nullptr;
        ReceiveCallback receiveEvent = nullptr;
        DisconnectedCallback disconnectedEvent = nullptr;
        int GetCount();
        int ReceiveFrom(int clientId, char *buffer, int size);
        int SendTo(int clientId, char *buffer, int size);
        int CloseClient(int clientId);
        int CloseAllClient();
        ~ManagedTcpServer();
    };
}
#endif