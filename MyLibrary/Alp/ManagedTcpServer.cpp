#ifndef ALP_MANAGEDTCPSERVER
#define ALP_MANAGEDTCPSERVER
#include "ManagedTcpServer.h"

namespace alp
{

    ManagedTcpServer::ManagedTcpServer(int port, int max) : _max(max)
    {
        tcpServer = new TcpServer(port);
        _listClient = new AkList<ManagedTcpClient>(max);
        _AthreadServer = new Athread(_Start);
        _disposed = 0;
    }

    int ManagedTcpServer::Start()
    {
        _AthreadServer->Then([](void *data)
                             { std::cout << "Stopped listening.\n"; });

        _AthreadServer->Run(this);
        return 1;
    }

    void ManagedTcpServer::_Start(void *data)
    {
        ManagedTcpServer *_this = (ManagedTcpServer *)data;

        _this->tcpServer->Start();
        _this->listeningStatus = LISTENING;

        if (_this->receiveEvent == nullptr)
        {
            perror("ManagedTcpServer -> Receive callback needs to be setted");
            exit(EXIT_FAILURE);
        }
        std::cout << "Start Listening...\n";
        while (_this->listeningStatus == LISTENING)
        {
            TcpClient *client = _this->tcpServer->Accept();
            if (client != nullptr)
            {
                if (_this->_listClient->GetCount() >= _this->_max)
                {
                    std::cout << "Server is full." << std::endl;
                    client->Close();
                    continue;
                }
                ManagedTcpClient *mClient = new ManagedTcpClient(client, _this->acceptEvent, _this->receiveEvent, [=](int clientId)
                                                                 {
                                                                     if (_this->_disposed == 0)
                                                                     {
                                                                         _this->_listClient->Remove(clientId);
                                                                     }
                                                                     _this->disconnectedEvent(clientId);
                                                                 });

                mClient->_clientId = _this->_listClient->Add(mClient);
                mClient->Init();
            }
            else
                client->Close();
        }

        _this->tcpServer->Stop();
    }

    int ManagedTcpServer::GetCount()
    {
        return _listClient->GetCount();
    }

    int ManagedTcpServer::ReceiveFrom(int clientId, char *buffer, int size)
    {
        return _listClient->Get(clientId)->Receive(buffer, size);
    }

    int ManagedTcpServer::SendTo(int clientId, char *buffer, int size)
    {
        return _listClient->Get(clientId)->Send(buffer, size);
    }

    int ManagedTcpServer::CloseClient(int clientId)
    {
        delete _listClient->Remove(clientId);
        return 1;
    }

    int ManagedTcpServer::CloseAllClient()
    {
   //     _listClient->ClearAndDispose();
        return 1;
    }

    ManagedTcpServer::~ManagedTcpServer()
    {

        // CloseAllClient();
        _disposed = 1;
        _listClient->ClearAndDispose();
        delete _AthreadServer;
        delete tcpServer;
        delete _listClient;
    }
}
#endif