#include "Dictionary.h"
int Tests(Dictionary *dict, int testNo);

int totalMemory = 0;
void *operator new(size_t size)
{
	totalMemory += size;
	std::cout << "malloc " << size << std::endl;
	void* ptr = malloc(size);
	printf("%p\n", ptr);
	return ptr;
}

void operator delete(void *memory, size_t size)
{
	totalMemory -= size;
	std::cout << "free " << size << std::endl;
	free(memory);
}

class deneme
{
public:
	int _value;
	std::string _str;
	deneme(int value, std::string str)
	{
		_value = value;
		_str = str;
	}
};

int main()
{
		Dictionary dictionary;
getchar();
		Tests(&dictionary, 2);

	while (1)
	{
		getchar();
	}

	return 0;
}

int Tests(Dictionary *dict, int testNo)
{

	switch (testNo)
	{
	case 1:
	{
		dict->Add(1, new deneme(11, "selam"));
		dict->Add(5, new deneme(55, "5454ea"));
		dict->Remove(5);
		deneme *den = (deneme *)dict->Find(1);
		if (den == 0x0)
			std::cout << "Not found";
		else
			std::cout << den->_str;
	}
	break;
	case 2:
		std::cout << "Eklemek icin tusa bas";
		getchar();
		for (int i = 0; i < 100000; i++)
		{
			dict->Add(i, new deneme(i, "selam"));
		}
		std::cout << "Silmek icin tusanbas";
		getchar();
		for (int i = 0; i < 100000; i++)
		{
			delete ((deneme *)dict->Find(i));
			dict->Remove(i);
		}
		break;
	default:
		break;
	}

	return 1;
}