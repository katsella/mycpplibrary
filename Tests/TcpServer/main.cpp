#include <iostream>
#include "Alp/ManagedTcpServer.h"
#include "Alp/ManagedTcpClient.h"
#include <string.h>

void receiveCallback(int clientId, char *buffer, int size);
int acceptCallback(int clientId);
void disconnectedCallback(int clientId);

int totalMemory = 0;
/*
void *operator new(size_t size)
{
    totalMemory += size;
    if (size == 288)
        std::cout << "malloc " << size << std::endl;
    //   std::cout << totalMemory << " +" << std::endl;
    return malloc(size);
}

void operator delete(void *memory, size_t size)
{
    totalMemory -= size;
    //   std::cout << "free " << size << std::endl;
    //  std::cout << totalMemory << " -" << std::endl;
    free(memory);
}

void operator delete[](void *memory)
{
   // totalMemory -= size;
    //   std::cout << "free " << size << std::endl;
    //  std::cout << totalMemory << " -" << std::endl;
    free(memory);
}
*/
alp::ManagedTcpServer *tcpServer;
int GetInt(char *msg)
{
    std::string data;

    while (1)
    {
        std::cout << msg;
        std::getline(std::cin, data);
        int checkDigit = 1;
        int i = 0;
        for (i = 0; data[i] != 0x0; i++)
        {
            if (!isdigit(data[i]) || i > 10)
            {
                checkDigit = 0;
                break;
            }
        }
        if (checkDigit && i != 0)
            break;
    }

    return std::stoi(data);
}

int main()
{

    while (1)
    {
        int x = GetInt((char *)"# ");
        if (x == 0)
        {
            delete tcpServer;
        }
        else if (x == 1)
        {
            tcpServer = new alp::ManagedTcpServer(9876, 1000);
            tcpServer->acceptEvent = acceptCallback;
            tcpServer->receiveEvent = receiveCallback;
            tcpServer->disconnectedEvent = disconnectedCallback;
            tcpServer->Start();
            std::cout << "Server Start Listening..." << std::endl;
        }
        else if (x == 2)
        {
            break;
        }
        else if (x == 3)
        {
            std::cout << "  --  " << totalMemory << std::endl;
        }
    }
    return 0;
}
int count = 0;

int acceptCallback(int clientId)
{
    count++;
    /*  if (count >= 500)
        tcpServer.listeningStatus = tcpServer.NOT_LISTENING;*/
    std::cout << "New Client Connected. " << clientId << " Total: " << std::endl;
    // tcpServer->CloseClient(clientId);
    /*char *buffer = new char[500];
    int size = tcpServer->ReceiveFrom(clientId, buffer, 500);
    std::cout << size;
    delete buffer;*/
    int tryCount = 0;
    char rec[500];

    while (1)
    {
        int size = tcpServer->ReceiveFrom(clientId, rec, 500);
        rec[size] = '\0';
        if (strcmp(rec, (char *)"1234") == 0)
            break;
        if (tryCount++ > 4)
            return -1;
    }

    char message[] = "Hoşgeldiniz\r\n";
    tcpServer->SendTo(clientId, message, sizeof(message) / sizeof(char));
    return 1;
}

void receiveCallback(int clientId, char *buffer, int size)
{
    if (size == 2 && buffer[0] == '\r' && buffer[1] == '\n')
        return;
    buffer[size] = '\0';
    std::cout << "new message(" << size << "): " << buffer << std::endl;
    char message[] = "Ok.\r\n";
    tcpServer->SendTo(clientId, message, sizeof(message) / sizeof(char));
}

void disconnectedCallback(int clientId)
{
    std::cout << "<--Disconnected " << std::endl;
}