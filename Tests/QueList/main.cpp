#include "Alp/StaticQueList.h"
#include <thread>

void TestAdd(alp::StaticQueList<int> *que, int size, int thId);

int main()
{
    alp::StaticQueList<int> que(999999);
    int thCount = 0;
    int loopCount = 0;

    std::cout << "Thread Count : ";
    std::cin >> thCount;

    std::thread thList[thCount];

    while (1)
    {
        std::cout << "Loop Count Each: ";
        std::cin >> loopCount;
        if (loopCount == 0)
            break;
        for (size_t i = 0; i < thCount; i++)
        {
            thList[i] = std::thread(TestAdd, &que, loopCount, i);
        }

        for (size_t i = 0; i < thCount; i++)
        {
            if (thList[i].joinable())
                thList[i].join();
        }

        std::cout << que.GetCount() << std::endl;
    }

    return 1;
}

void TestAdd(alp::StaticQueList<int> *que, int size, int thId)
{
    int count = 0;
    int add = 0;
    while (count < size)
    {
        count++;
        int value = rand();
        if (value % 5 == 0)
        {
            que->GetLast();
            add--;
        }
        else if (value % 5 == 1)
        {
            que->GetFirst();
            add--;
        }
        else
        {
            que->Add(rand() % 1000);
            add++;
        }
    }
    std::cout << "Thread-" << thId << " " << add << " item" << std::endl;
}