#include <stdio.h>
#include <sys/socket.h>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <Alp/LinkedList.h>
#include <thread>
#include <string>
#define PORT 9876

int GetInt(char *msg)
{
    std::string data;

    while (1)
    {
        std::cout << msg;
        std::getline(std::cin, data);
        int checkDigit = 1;
        for (int i = 0; data[i] != 0x0; i++)
        {
            if (!isdigit(data[i]) || i > 10)
            {
                checkDigit = 0;
                break;
            }
        }
        if (checkDigit)
            break;
    }

    return std::stoi(data);
}

int Connect(int count);

int main()
{
    int total = 0;
    while (1)
    {
        int secim = -1;
        int threadCount = 1;

        threadCount = GetInt((char *)"Threat Count: ");

        while (secim != 0)
        {
            alp::LinkedList<std::thread *> thList;
            total = 0;
            int repeatCount = GetInt((char *)"Repeat Count: ");
            for (int i = 0; i < threadCount; i++)
            {
                thList.Push(new std::thread(Connect, repeatCount / threadCount));
                total += repeatCount / threadCount;
            }

            thList.Foreach([](std::thread *item)
                           {
                               std::thread *th = (std::thread *)(item);
                               if (th->joinable())
                                   th->join();
                               delete th;
                           });
            printf("\nTotal %d Client Connected.\n", total);
        }
    }
}

int Connect(int count)
{
    for (size_t i = 0; i < count; i++)
    {
        int sock = 0, valread;
        struct sockaddr_in serv_addr;
        char *hello = (char *)"Hello from client";
        char buffer[1024] = {0};
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            printf("\n Socket creation error \n");
            return -1;
        }

        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(PORT);

        // Convert IPv4 and IPv6 addresses from text to binary form
        if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
        {
            printf("\nInvalid address/ Address not supported \n");
            return -1;
        }

        if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        {
            printf("\nConnection Failed \n");
            return -1;
        }

        send(sock, hello, strlen(hello), 0);
        send(sock, hello, strlen(hello), 0);
        printf(".");
        // valread = read(sock, buffer, 1024);
        //  if (rand() % 2 == 1)
     //   close(sock);
    }

    return 0;
}