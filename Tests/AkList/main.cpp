#include <iostream>
#include "Alp/AkList.h"
#include "Alp/StaticQueList.h"
#include <thread>

void wait(char msg[20])
{
    printf("%s starting...\n", msg);
    getchar();
}

class deneme
{
public:
    int _x;
    char *_data;
    deneme(int x, char *data) : _x(x), _data(data)
    {
    }
};

int main()
{
    int size = 10000;
    alp::StaticQueList<std::thread> thList(size);

    alp::AkList<deneme> *_list = new alp::AkList<deneme>(size);
    deneme *den;
    for (size_t i = 0; i < 20; i++)
    {
        thList.Add(new std::thread([&]() {
            int x;
            for (size_t i = 0; i < 100; i++)
            {
                switch (rand() % 2)
                {
                case 0:
                    _list->Add(new deneme(i, (char *)"selam"));
                    break;
                case 1:
                    if (_list->GetCount() == 0)
                        break;
                    x = rand() % (_list->GetCount());
                    den = _list->Remove(x);
                    if (den != nullptr)
                    {
                        delete den;
                        std::cout << "   ---   " << x << std::endl;
                    }
                    break;

                default:
                    break;
                }
            }
        }));
    }

        wait((char *)"Delete list");
    _list->ClearAndDispose();
    delete (_list);

    while (thList.GetCount() > 0)
    {
        auto a = thList.GetFirst();
        a->join();
        delete a;
    }

    wait((char *)"Bitti");
    return 0;
}