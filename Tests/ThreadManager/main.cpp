// #include "tAlp/ThreadManager.h"
#include <iostream>
#include "tAlp/Athread.h"
#include "Alp/Later.h"
#include "Alp/Later.h"

int total = 0;
/*
void *operator new(size_t size)
{
    // std::cout << "+" << size << std::endl;
    total += size;
    return malloc(size);
}

void operator delete(void *ptr, size_t size)
{
    // std::cout << "-" << size << std::endl;
    total -= size;
    free(ptr);
}

void operator delete[](void *ptr, size_t size)
{
    // std::cout << "-" << size << std::endl;
    total -= size;
    free(ptr);
}
*/
void test(void *a)
{
    //   for (int i = 0; i < 1000; i++)
    {
        usleep(1000000);
        std::cout << (char *)a;
    }
}

int main()
{
    {
        alp::Later::Add([]()
                        { std::cout << "Selam\n"; });

        for (size_t i = 0; i < 1000; i++)
        {
            std::cin.get();
            for (size_t i = 0; i < 100; i++)
            {

                alp::Athread *athread = new alp::Athread(test);
                athread->Then([=](void *data)
                              {
                                  delete athread;
                                  std::cout << "---" << std::endl;
                              });
                athread->Run((char *)"Selam");
                // usleep(10);
            }
            std::cin.get();
            std::cout << "bitti -- " << total;
        }

        std::cin.get();
        return 0;
    }
}