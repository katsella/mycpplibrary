#include <iostream>
#include "Alp/ManagedTcpClient.h"

int main()
{
    struct sockaddr_in sockaddr;
    alp::TcpClient *tcpClient = new alp::TcpClient(4, 15, sockaddr);
    alp::ManagedTcpClient *managedClient = new alp::ManagedTcpClient(tcpClient, nullptr, nullptr);
    managedClient->StartReceive();
    delete managedClient;

    std::cin.get();
}